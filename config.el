;;
;;; Packages
;; 
;; (setq org-super-agenda-groups '((:name "Today"
;;                                   :time-grid t
;;                                   :scheduled today)
;;                            (:name "Due today"
;;                                   :deadline today)
;;                            (:name "Important"
;;                                   :priority "A")
;;                            (:name "Overdue"
;;                                   :deadline past)
;;                            (:name "Due soon"
;;                                   :deadline future)
;;                            (:name "Big Outcomes"
;;                                   :tag "bo"))
;; (set-popup-rule! "^\\*Org Agenda" :side 'bottom :size 0.90 :select t :ttl nil))

;; org-sticky-header
  (setq org-sticky-header-full-path 'full)
  (add-hook! 'org-mode-hook
  (org-sticky-header-mode))


;;   (setq org-brain-path (expand-file-name "brain" org-directory))
;;   ;; For Evil users
;;   (with-eval-after-load 'evil
;;     (evil-set-initial-state 'org-brain-visualize-mode 'emacs))
;;   :config
;;   (setq org-id-track-globally t)
;;   (setq org-id-locations-file (expand-file-name ".org-id-locations" org-directory))
;;   (setq org-brain-visualize-default-choices 'all)
;;   (setq org-brain-title-max-length 12)
;;

;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here

(add-to-list 'default-frame-alist '(inhibit-double-buffering . t))
;; keyboard macros
(fset 'ko-punch-login
   (lambda (&optional arg) "Keyboard macro." (interactive "p") (kmacro-exec-ring-item (quote ([48 79 42 42 42 32 escape 32 111 116 108 32 111 99 112] 0 "%d")) arg)))
(setq
 doom-font (font-spec :family "Source Code Pro" :size 16)
 doom-variable-pitch-font (font-spec :family "Source Code Pro" :size 18)
 doom-big-font (font-spec :family "Source Code Pro" :size 19))

(when IS-LINUX
  (font-put doom-font :size 16)
  (font-put doom-font :weight 'Bold)
  (add-hook 'window-setup-hook #'toggle-frame-maximized))

(when IS-WINDOWS
;; (setq doom-font (font-spec :family "Source Code Pro" :size 14))
  (font-put doom-font :size 14)
  (font-put doom-font :weight 'Bold)
  (add-hook 'window-setup-hook #'toggle-frame-maximized)
  )

(when IS-MAC
  (setq ns-use-thin-smoothing t)
  (add-hook 'window-setup-hook #'toggle-frame-maximized))


 ;; Also install this for ligature support:
 ;; https://github.com/tonsky/FiraCode/files/412440/FiraCode-Regular-Symbol.zip


;; ozzy
;;
;;; Modules
;; (setq deft-extensions "")
;; (setq deft-recursive t)
;; app/rss
(add-hook! 'elfeed-show-mode-hook (text-scale-set 2))
;; lang/org
(setq global-auto-revert-mode t)
(setq auto-revert-use-notify nil)
(setq org-log-into-drawer t)
(when IS-WINDOWS (setq org-directory "e:/Org/"))
(when IS-LINUX (setq org-directory "~/Org/"))
(after! org
  (add-to-list 'org-modules 'org-habit t)
  (setq org-log-into-drawer t)
  (setq org-default-notes-file (expand-file-name "journal/Notes2019.org" org-directory))
  (setq org-agenda-files (list org-directory)
        org-todo-keywords
        '((sequence "TODO(t!)" "IN-PROCESS(i!)" "SOMEDAY(s!)" "|" "DONE(d!)")
          (sequence "[ ](T)" "[-](p)" "[?](m)" "|" "[X](D)")
          (sequence "NEXT(n!)" "WAITING(w!)" "LATER(l!)" "|" "CANCELLED(c!)")
          )
        org-todo-keyword-faces
        '(("WAITING" :inherit bold)
          ("LATER" :inherit (warning bold)
           )))
;; (after! org-agenda
  (setq org-agenda-skip-scheduled-if-done t
      org-agenda-skip-deadline-if-done t
      org-agenda-include-deadlines t
      org-agenda-block-separator nil
      org-agenda-compact-blocks t
      org-agenda-start-day nil ;; i.e. today
      org-agenda-span 1
      org-agenda-start-on-weekday nil
  )

)

;;  (org-super-agenda-mode)
;; (let ((org-super-agenda-groups
;;        '((:auto-group t))))
;;   (org-agenda-list))

(let ((org-agenda-span 'day)
      (org-super-agenda-groups
       '((:name "Time grid items in all-uppercase with RosyBrown1 foreground"
                :time-grid t
                :transformer (--> it
                                  (upcase it)
                                  (propertize it 'face '(:foreground "RosyBrown1"))))
         (:name "Priority >= C items underlined, on black background"
                :face (:background "black" :underline t)
                :not (:priority>= "C")
                :order 100))))
  (org-agenda nil "a"))
;; (let ((org-super-agenda-groups
;;        '((:auto-category t))))
;;   (org-agenda-list))
(let ((org-super-agenda-groups
       '((:auto-map (lambda (item)
                      (-when-let* ((marker (or (get-text-property 0 'org-marker item)
                                               (get-text-property 0 'org-hd-marker item)))
                                   (file-path (->> marker marker-buffer buffer-file-name))
                                   (directory-name (->> file-path file-name-directory directory-file-name file-name-nondirectory)))
                        (concat "Directory: " directory-name)))))))
  (org-agenda-list))
(setq org-ellipsis " ▶ "
      ;; org-bullets-bullet-list '("#")
      ;; org-log-done 'time
      ;; org-fast-tag-selection-single-key t
      org-use-speed-commands t
      org-tag-persistent-alist
      '((:startgroup)
        ("HOME" . ?H)
        ("LEARNING" . ?l)
        ("GARDEN" . ?g)
        ("WORK" . ?w)
        (:endgroup)
        (:startgroup)
        ("OS" . ?o)
        ("DOOM" . ?d)
        ("PROG" . ?p)
        ("SPACEMACS" . ?s)
        (:endgroup)
        (:startgroup)
        ("EASY" . ?a)
        ("MEDIUM" . ?b)
        ("HARD" . ?c)
        ("HANDLED" . ?h)
        (:endgroup)
        ("URGENT" . ?u)
        ("KEY" . ?k)
        ("BONUS" . ?B)
        ("FIX" . ?f)
        ("REFILE" . ?r)
        ("noexport" . ?n))
      org-tag-faces
      '(("HOME" . (:foreground "GoldenRod" :weight bold))
        ("LEARNING" . (:foreground "GoldenRod" :weight bold))
        ("GARDEN" . (:foreground "DarkGreen" :weight bold))
        ("WORK" . (:foreground "GoldenRod" :weight bold))
        ("REFILE" . (:foreground "Orange" :weight bold))
        ("OS" . (:foreground "IndianRed1" :weight bold))
        ("DOOM" . (:foreground "IndianRed1" :weight bold))
        ("PROG" . (:foreground "IndianRed1" :weight bold))
        ("URGENT" . (:foreground "Red" :weight bold))
        ("KEY" . (:foreground "Red" :weight bold))
        ("EASY" . (:foreground "green" :weight bold))
        ("MEDIUM" . (:foreground "yellow" :weight bold))
        ("HARD" . (:foreground "OrangeRed" :weight bold))
        ("BONUS" . (:foreground "GoldenRod" :weight bold))
        ("noexport" . (:foreground "LimeGreen" :weight bold))
        ("FIX" . (:foreground "IndianRed1" :weight bold))
        ))


;; company
;; Make autocomplete work with org (C-x C-o)
(defun add-pcomplete-to-capf ()
  (add-hook 'completion-at-point-functions 'pcomplete-completions-at-point nil t))
(add-hook 'org-mode-hook #'add-pcomplete-to-capf)
(global-company-mode)
;;
;;; Packages
;; 
;; (setq org-super-agenda-groups '((:name "Today"
;;                                   :time-grid t
;;                                   :scheduled today)
;;                            (:name "Due today"
;;                                   :deadline today)
;;                            (:name "Important"
;;                                   :priority "A")
;;                            (:name "Overdue"
;;                                   :deadline past)
;;                            (:name "Due soon"
;;                                   :deadline future)
;;                            (:name "Big Outcomes"
;;                                   :tag "bo"))
;; (set-popup-rule! "^\\*Org Agenda" :side 'bottom :size 0.90 :select t :ttl nil))

;; org-sticky-header
  (setq org-sticky-header-full-path 'full)
  (add-hook! 'org-mode-hook
  (org-sticky-header-mode))


;;   (setq org-brain-path (expand-file-name "brain" org-directory))
;;   ;; For Evil users
;;   (with-eval-after-load 'evil
;;     (evil-set-initial-state 'org-brain-visualize-mode 'emacs))
;;   :config
;;   (setq org-id-track-globally t)
;;   (setq org-id-locations-file (expand-file-name ".org-id-locations" org-directory))
;;   (setq org-brain-visualize-default-choices 'all)
;;   (setq org-brain-title-max-length 12)
;; 
(after! ivy
  ;; OVERRIDE
  (advice-add #'+ivy/projectile-find-file :override #'counsel-git)

  ;; ivy/flx already sets ivy-re-builders-alist

  ;; Use minibuffer to display ivy functions
  (dolist (fn '(+ivy/switch-workspace-buffer
                ivy-switch-buffer))
    (setf (alist-get fn ivy-display-functions-alist) #'ivy-display-function-fallback)))


;;
;;; Custom
(setq global-visual-line-mode t)
(setq menu-bar-mode t)
(load! "autoload/time")
(load! "autoload/bindings")
(load! "autoload/text")
(load! "autoload/capture")
(load! "autoload/evil")
