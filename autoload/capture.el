

(push '("n" "Personal notes" entry
         (file+headline +org-capture-notes-file "Inbox")
         "* %u %?\n%i" :prepend t :kill-buffer t)
      org-capture-templates)
(push '("p" "Templates for projects")
      org-capture-templates)
(push '("pt" "Project todo" entry    ; {project-root}/todo.org
         (file+headline +org-capture-project-todo-file "Inbox")
         "* TODO %?\n%i" :prepend t :kill-buffer t)
      org-capture-templates)
(push '("pn" "Project notes" entry   ; {project-root}/notes.org
         (file+headline +org-capture-project-notes-file "Inbox")
         "* TODO %?\n%i" :prepend t :kill-buffer t)
      org-capture-templates)
(push '("pc" "Project changelog" entry ; {project-root}/changelog.org
         (file+headline +org-capture-project-notes-file "Unreleased")
         "* TODO %?\n%i" :prepend t :kill-buffer t)
      org-capture-templates)

        ;; Will use {project-root}/{todo,notes,changelog}.org, unless a
        ;; {todo,notes,changelog}.org file is found in a parent directory.
(setq org-capture-project-notes-file "~/Org/notes.org")
(setq org-capture-templates
      '(("t"             ; hotkey
         "Todo list item" ; name
            entry            ; type
            ; heading type and title
            (file+headline "~/Org/Tasks.org" "Tasks")
;;         "* TODO %?\n %i\n %a") ; template
            (file "~/.doom.d/org-templates/todo.orgcaptmpl"))

        ))
(push '("j" "Journal entry"
            entry (file+olp+datetree org-default-notes-file)
            (file "~/.doom.d/org-templates/journal.orgcaptmpl"))
org-capture-templates
)
(push '("b" "Tidbit: quote, zinger, one-liner or textlet"
              entry
              (file+headline org-default-notes-file "Tidbits")
              (file "~/.doom.d/org-templates/tidbit.orgcaptmpl"))
org-capture-templates
)
(push  '("k" "breaK" entry (file+olp+datetree org-default-notes-file)
             "* %?\n  %a\n  %K"  :clock-in t :clock-resume t
      )
org-capture-templates
)
(push '("h" "Habit Daily" entry (file+olp "~/Org/Tasks.org" "Tasks" "RepeatingTasks" )
       "* TODO %? \n SCHEDULED: <%<%Y-%m-%d %a .+1d>>\n:PROPERTIES:\n:CREATED: %U\n:STYLE: habit\n:REPEAT_TO_STATE: TODO \n:LOGGING: DONE(!)\n:ARCHIVE: %%s_archive::* Habits\n:END:\n%U\n%a\n %K"
       )
org-capture-templates
)
(push '("w" "Web Link" entry (file+olp "~/Org/Diary.org" "Refile" "Web")
       "* [[%c][%?]] \n%K\n  :PROPERTIES:\n  :CREATED:   %U\n  :END:" :unnarrowed t ;; :clock-in t :clock-resume t
       )
org-capture-templates
)

(push '("H" "Home" entry (file+olp "~/Org/Diary.org" "Home")
       "* %? \n:LOGBOOK:\n %T \n:END:\n  %a\n  %K"  :clock-in t :clock-resume t
       )
org-capture-templates
)
(push '("l" "Link to current header" entry (file+olp "~/Org/Diary.org" "Refile" "Links")
       "* %a %? \n%K\n  :PROPERTIES:\n  :CREATED:   %U\n  :END:" :unnarrowed t ;; :clock-in t :clock-resume t
       )
org-capture-templates
)
(server-start)
(add-to-list 'load-path "~/.emacs.d/.local/packages/elpa/org-plus-contrib-20190603")
(require 'org-protocol)
(push '("P" "Protocol" entry (file+headline ,(concat org-directory "notes.org") "Inbox")
        "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")

org-capture-templates)

(push '("L" "Protocol Link" entry (file+headline ,(concat org-directory "notes.org") "Inbox")
        "* %? [[%:link][%:description]] \nCaptured On: %U")

org-capture-templates
)
(defun gds/next-month ()
   "Get next month's date as a string."
   (cl-destructuring-bind (sec min hour day month year dow dst zone)
       (decode-time (current-time))
     (format-time-string "%F" (encode-time 0 0 0 day (+ 1 month) year))))

(push '("4" "Todo within a month" entry (file+olp "~/Org/Tasks.org" "Tasks" "ScheduledTasks")
       "* TODO %? \n  :PROPERTIES:\n  :EXPIRY:   [%(gds/next-month)]\n  :END:\n  %a\n  %K"
       )
org-capture-templates
)
(defun gds/in-a-fortnight ()
   "Get next fortnight's date as a string."
   (format-time-string "%F" (time-add (current-time) (days-to-time 14))))

(push '("3" "Todo within a fortnight" entry (file+olp "~/Org/Tasks.org" "Tasks" "ScheduledTasks")
       "* TODO %? \n  :PROPERTIES:\n  :EXPIRY:   [%(gds/in-a-fortnight)]\n  :END:\n  %a\n  %K"
       )
org-capture-templates
)
(defun gds/next-week ()
   "Get next week's date as a string."
   (format-time-string "%F" (time-add (current-time) (days-to-time 7))))

(push '("2" "Todo within a week" entry (file+olp "~/Org/Tasks.org" "Tasks" "ScheduledTasks")
       "* TODO %? \n  :PROPERTIES:\n  :EXPIRY:   [%(gds/next-week)]\n  :END:\n  %a\n  %K"
       )
org-capture-templates
)
(defun gds/tomorrow ()
   "Get tomorrow's date as a string."
   (format-time-string "%F" (time-add (current-time) (days-to-time 1))))

(push '("1" "Todo by tomorrow" entry (file+olp "~/Org/Tasks.org" "Tasks" "ScheduledTasks")
        "* TODO %? \n SCHEDULED:   <%(gds/tomorrow)>\n:PROPERTIES:\n:CREATED: %U\n:END:\n  %a\n  %K"
      )
org-capture-templates
)
