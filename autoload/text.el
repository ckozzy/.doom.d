;;;  -*- lexical-binding: t; -*-


(after! org
;; test tangle
(setq org-agenda-files (list org-directory)
      org-ellipsis " ▼ "

      ;; The standard unicode characters are usually misaligned depending on the
      ;; font. This bugs me. Markdown #-marks for headlines are more elegant.
;; ozzy      org-bullets-bullet-list '("#")
)

;; (setq org-refile-targets '((nil :maxlevel . 9)
;;                                 (org-agenda-files :maxlevel . 9)))
;; (setq org-outline-path-complete-in-steps nil)         ; Refile in a single go
;; (setq org-refile-use-outline-path t)                  ; Show full paths for refiling

(setq org-refile-targets (quote (("Diary.org" :maxlevel . 2)
                                ("Tasks.org" :level . 2)
                                ("Someday.org" :level . 2))))
;; 

)
