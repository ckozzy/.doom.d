;;; private/my/bindings.el -*- lexical-binding: t; -*-
;; Stolen from ztlevi but not all
(map!
 ;; overrides other minor mode keymaps (just for non-evil)
 (:map override ;; general-override-mode-map
      ;; Easier window movement
      :nvi "C-h" 'evil-window-left
      :nvi "C-j" 'evil-window-down
      :nvi "C-k" 'evil-window-up
      :nvi "C-l" 'evil-window-right
 ;; Restore somewhat common navigation
 ;; stolen from ztlevi
;;       "M-l" #'goto-line
;;  ;; Restore OS undo, save, copy, & paste keys (without cua-mode, because
;;  ;; it imposes some other functionality and overhead we don't need)
;;       "M-z" #'undo
;;       "M-Z" #'redo ;; standard
;;       "M-c" (if (featurep 'evil) #'evil-yank #'copy-region-as-kill)
;;       "M-v" #'yank-with-delete-region  ;; standard but failed
;;       "M-s" #'evil-write-all   ;; standard but like
;;       ;; Buffer-local font scaling
;;       "M-0" (λ! (text-scale-set 0))
;;       "M-=" #'text-scale-increase
;;       "M--" #'text-scale-decrease
;;       ;; Conventional text-editing keys & motions
;;       "M-a" #'mark-whole-buffer
;; ;;      :gni [M-RET]    #'+default/newline-below
;; ;;      :gni [M-S-RET]  #'+default/newline-above
;;       :gi  [M-backspace] #'backward-kill-word
;; ;;      :gi  [M-left]      #'backward-word
;; ;;      :gi  [M-right]     #'forward-word
;;       ;; Swiper
;;       "M-f" #'swiper
;;       "C-s" #'swiper

;;       "M-q"   (if (daemonp) #'delete-frame #'save-buffers-kill-terminal)
;;       "M-p"   #'counsel-git
;;       "C-S-p" #'counsel-git
;;       "M-y"   #'helm-show-kill-ring
;;       "C-h m" #'describe-mode

;;       "M-`"   #'other-frame
;;       "C-M-o" #'other-frame
;;       ;; fix OS window/frame navigation/manipulation keys
;;       "M-w" #'delete-window
;;       "M-W" #'delete-frame
;;       "M-n" #'+default/new-buffer
;;       "M-N" #'make-frame
;;       "C-M-f" #'toggle-frame-fullscreen

;;       :gi  [M-left]      #'backward-word
;;       :gi  [M-right]     #'forward-word
;;       ;; Help
;;       "C-h h" nil
;;       "C-h C-k" #'find-function-on-key
;;       "C-h C-f" #'find-function-at-point
;;       "C-h C-v" #'find-variable-at-point
;;       "<f8>"    #'describe-mode
;;       ;; Others
;;       "M-e" #'+ivy/switch-workspace-buffer
;;       "C-M-\\" #'indent-region-or-buffer
;;       "M-m" #'kmacro-call-macro
;;       "M-/" #'doom/toggle-comment-region-or-line
      )
)

(map!
   ;; Unix text-editing keys & motions
   :gi "C-n" #'next-line
   :gi "C-p" #'previous-line
   :gi "C-b" #'backward-char
   :gi "C-f" #'forward-char
   :gi "C-k" #'kill-line
   :gi "C-d" #'delete-forward-char

   :v "<del>" (kbd "\"_d")
   :v "<backspace>" (kbd "\"_d")

   :gnmvi "C-e" #'doom/forward-to-last-non-comment-or-eol
   ;; :gnmvi "C-a" #'doom/backward-to-bol-or-indent
   :gnmvi "M-." #'+lookup/definition
   :ne "M-/" #'comment-or-uncomment-region
   :gnmvi  "C-a" #'avy-got-line
   :gnmvi  "C-;" #'avy-goto-char
  ;; temp
   (:prefix "C-x"
     :n "e"  #'pp-eval-last-sexp)
   (:prefix "C-c"
     :ni "/" #'company-files
     :desc "Text properties at point" :nmv "f" (λ! (message "%S" (text-properties-at (point))))))

;; leader/localleader is not compatible with :gnvmi
(map! :leader
      :desc "counsel-M-x" :nmv "SPC" #'counsel-M-x


      (:prefix "/"                      ; search
        :desc "Project"   "/" #'+ivy/project-search
        :desc "Project (hidden)" "h" #'+ivy/project-search-with-hidden-files
        :desc "Comments"  "c" #'counsel-imenu-comments
        :desc "Jump"  "j" #'counsel-imenu-comments)
      ;; from ztlevi
      (:prefix ("j" . "jump")
        "j" #'evil-avy-goto-char
        "l" #'evil-avy-goto-line
        "b" #'avy-pop-mark)
      (:prefix "t"                      ; toggle
        "c" #'centered-window-mode
        "r" #'rjsx-mode
        "D" #'toggle-debug-on-error
;;        "D" #'+my/realtime-elisp-doc
        "L" #'toggle-truncate-lines
        "S" #'size-indication-mode
        :desc "Ivy Rich Mode" "I" #'ivy-rich-mode
        "v" #'visual-line-mode)
      (:desc "Link"        :prefix           "l"
         :desc "store"            :nv         "s" #'org-store-link
         :desc "insert"           :nv         "i" #'org-insert-link
;;         :desc "headline"         :nv         "h" #'aj/insert-link-into-org-heading
;;         :desc "list"             :nv         "l" #'aj/insert-link-into-org-list-item
         :desc "open"             :nv         "o" #'org-open-at-point
         )
      (:prefix "n"
            :desc "Deft Find File"  :nv  "D" #'deft-find-file)
      
)


(map!
 (:after org
   (:map org-mode-map
     "M-t" #'org-todo
     :localleader
     "s" #'org-schedule
     "w" #'org-refile
     "z" #'org-add-note
     "L" #'org-toggle-link-display
     "A" #'org-archive-subtree
     "T" #'org-babel-tangle
    )
   (:map override
     :gni [M-return] #'org-meta-return
     :gni [C-return] #'org-insert-heading-respect-content
     :ni  [M-right]  #'org-metaright
     :ni  [M-left]   #'org-metaleft
   
     )))


;; ozzy bindings
(map! :leader
      :prefix "o"
      "d" nil)

(map! (:map override
 (:after org
   (:map org-mode-map
     :leader
      (:prefix "o"
         (:prefix ("d" . "Date")
           "d" #'ozzy-date-time
           "l" #'ozzy-date-long
           "t" #'ozzy-time
           "s" #'ozzy-date-short
           "S" #'ozzy-date-time-short
          )
         (:prefix ("c" . "Clock")
           "c" #'org-clock-in
           "C" #'org-clock-out
           "g" #'org-clock-goto
           "p" #'bh/punch-in
           "P" #'bh/punch-out)
         )))))
 (map! :leader
       (:prefix ("a" . "app")
         "a" #'org-agenda
         "A" #'org-agenda-file-to-front
         "e" #'eshell/open
         "s" #'shell-command)

       (:prefix ("w")
         "/" #'evil-window-vsplit ))
;; test ozzy
